module.exports = {
  siteMetadata: {
    title: 'Gatsby With Apollo',
  },
  plugins: [
    {
      resolve: 'gatsby-source-graphql',
      options: {
        typeName: 'MONITORIAS',
        fieldName: 'monitorias',
        url: 'https://monitoriasuniversitarias-dev.herokuapp.com/api/',
      },
    },
    {
      resolve: 'gatsby-plugin-apollo',
      options: {
        uri: 'https://monitoriasuniversitarias-dev.herokuapp.com/api/'
      }
    }
  ],
};
