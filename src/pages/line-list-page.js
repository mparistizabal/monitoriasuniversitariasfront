import React from 'react';

import LineList from '../Components/Templates/LineList';
import Header from '../Components/Atoms/Header';
import Title from '../Components/Atoms/Title';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  base: {
    padding: '20px 30px 20px 30px',
  },
}));

const LineListPage = () => {
  const styles = useStyles();
  return (
    <>
      <Header />
      <Title text="Lineas de aprendizaje" />
      <div className={styles.base}>
        <LineList />
      </div>
    </>
  );
};

export default LineListPage;
