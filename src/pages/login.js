import React from 'react';

import { window } from 'browser-monads';

import Header from '../Components/Atoms/Header';
import LoginCard from '../Components/Atoms/loginCard';

import Icon from '../assets/icon-blue.jpg';
import IconText from '../assets/text-white.jpg';

import { Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  base: {
    height: '100vh',
    backgroundColor: '#032440',
  },
  grid: {
    marginTop: '10%',
    marginBottom: '10%',
  },
  fieldsContainer: {
    padding: 20,
  },
  iconsContainer: {
    paddingleft: 20,
    paddingRight: 20,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  icon: {
    width: 200,
    border: 'none',
  },
  iconText: {
    width: 500,
  },
}));

const Login = () => {
  const styles = useStyles();


  const isDisplayed = () => {
    let display = false;
    if (typeof window !== undefined) {
      display = window.screen.width >= 960
    }
    return display;
  }


  return (
    <div className={styles.base}>
      <Header />
      <Grid
        className={styles.grid}
        container
        spacing={0}
        direction="row"
        justify="center"
      >
        {isDisplayed() ? (
          <Grid item sm={0} md={8} xs={12} className={styles.iconsContainer}>
            <img className={styles.icon} src={Icon} />
            <img className={styles.iconText} src={IconText} />
          </Grid>
        ) : null}
        <Grid item sm={8} md={4} xs={12} className={styles.fieldsContainer}>
          <LoginCard />
        </Grid>
      </Grid>
    </div>
  );
};

export default Login;
