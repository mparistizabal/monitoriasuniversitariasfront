/* Imports */

/* Components */

/* React */
import React from 'react';
import EditSubject from '../Components/Templates/EditSubject'
import Header from '../Components/Atoms/Header';
import Title from '../Components/Atoms/Title';

/* Edit Subject Form Page */
const EditSubjectPages = ({ location }) => (
  <>
    <Header />
    <Title text="Formulario de edición de tema" />
    {location.state && location.state.idSubject && (
      <EditSubject
        idSubject={location.state.idSubject}
        nameLine={location.state.nameLine}
        parentId={location.state.parentId}
      />
    )}
  </>
);

/* Export */
export default EditSubjectPages;