/* Imports */

/* Components*/
import EditLine from '../Components/Templates/EditLine';
import Header from '../Components/Atoms/Header';
import Title from '../Components/Atoms/Title';

/* React */
import React from 'react';

/* Edit Learning Line Form Page */
const EditLearningLine = ({ location }) => (
  <>
    <Header />
    <Title text="Formulario de edición de linea de aprendizaje" />
    {location.state && location.state.idLine && (
      <EditLine idLine={location.state.idLine} />
    )}
  </>
);

/* Export */
export default EditLearningLine;