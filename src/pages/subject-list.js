/* Imports */

/* Components */
import SubjectList from '../Components/Templates/SubjectList';

/* React */
import React from 'react';

import Header from '../Components/Atoms/Header';
import Title from '../Components/Atoms/Title';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  base: {
    padding: '20px 30px 20px 30px',
  },
}));

/* List Of All Subjetcs By Learning Line Page */
const SubjectListPage = ({ location }) => {
  const styles = useStyles();
  return (
    <>
      <Header />
      <Title text={location.state ? location.state.name : ''} />
      {location.state && location.state.id && location.state.name && (
        <div className={styles.base}>
          <SubjectList id={location.state.id} name={location.state.name} />
        </div>
      )}
    </>
  );
};

/* Export */
export default SubjectListPage;
