import React from 'react';

import Header from '../Components/Atoms/Header';

import { Grid, Button } from '@material-ui/core';

import { makeStyles } from '@material-ui/core';

import Icon404 from '../assets/error-404.png';

const Styles = makeStyles(theme => ({
  base: {
    height: '100vh',
    backgroundColor: '#032440',
  },
  grid: {
    marginTop: '10%',
    marginBottom: '10%',
  },
  iconContainer: {
    paddingleft: 20,
    paddingRight: 20,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  messageContainer: {
    color: '#fff',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: '40px',
  },
  title: {
    fontWeight: 'bold',
    fontSize: '43px',
    textAlign: 'center',
  },
  buttonContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  button: {
    backgroundColor: '#ec5252',
    marginTop: 20,
    marginLeft: 10,
    marginRight: 10,
    paddingLeft: '15px',
    paddingRight: '15px',
    fontWeight: 'bold',
    color: '#fff',
    fontSize: 13,
    paddingTop: 10,
    boxShadow: '1px 2px 5px #777',
    '&:hover': {
      backgroundColor: '#FF3344',
    },
  },
}));

const Page404 = () => {
  const styles = Styles();
  return (
    <div className={styles.base}>
      <Header />
      <Grid
        className={styles.grid}
        container
        spacing={0}
        direction="row"
        justify="center"
      >
        <Grid item sm={8} md={6} xs={12} className={styles.messageContainer}>
          <h2 className={styles.title}>La página buscada no existe.</h2>
          <p>Te invitamos a navegar entre:</p>
          <div className={styles.buttonContainer}>
            <Button className={styles.button}>Inicio</Button>
            <Button className={styles.button}>Quienes Somos</Button>
          </div>
        </Grid>
        <Grid item sm={0} md={6} xs={12} className={styles.iconContainer}>
          <img className={styles.icon} src={Icon404} />
        </Grid>
      </Grid>
    </div>
  );
};

export default Page404;
