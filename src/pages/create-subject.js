/* Imports */

/* Components */
import TCreateSubject from '../Components/Templates/CreateSubject';

/* Rect */
import React from 'react';
import Header from '../Components/Atoms/Header';
import Title from '../Components/Atoms/Title';

/* Create Subject Form Page */
const createSubjectPages = ({ location }) => (
  <>
    <Header />
    <Title text="Formulario de tema" />
    {location.state && location.state.idLine && location.state.nameLine && (
      <TCreateSubject
        idLine={location.state.idLine}
        nameLine={location.state.nameLine}
      />
    )}
  </>
);

/* Export */
export default createSubjectPages;