/* Imports */

/* React */
import React from 'react';

import Header from '../Components/Atoms/Header';
import Title from '../Components/Atoms/Title';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  base: {
    padding: '20px 30px 20px 30px',
  },
  paper: {
    marginTop: theme.spacing(8),
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(5),
    borderBottom: '0px solid transparent',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  text: {
    marginTop: '30px',
    textAlign:'justify'
  },
}));

const message = (status) => {
  switch (status) {
    case 'PRESELECTION':
      return 'El estado de tu postulación se notificará al correo electrónico anteriormente registrado';
    case 'DISCARDED':
      return 'Lo sentimos tu solicitud fue rechazada, vuelve a regstrarte para poder postularte de nuevo';
    case 'TEST':
      return 'Bienvenido a Monitorias Universitarias. Te encuentras en estado de prueba';
    case 'SELECTED':
      return 'Felicitaciones, tu solicitud ha sido aprobada. Bienvenido a Monitorias Universitarias';
  }
};

const PostulationStatusPage = ({ location }) => {
  const classes = useStyles();
  return (
    <>
      <Header />
      <Title text="Estado de postulación" />
      <Paper className={classes.paper}>
        {location.state && location.state.id && location.state.name && (
          <Typography
            component="h1"
            variant="h5"
            align="justify"
            className={classes.text}
          >
            {message(location.state.name)}
          </Typography>
        )}
      </Paper>
    </>
  );
};

/* Export */
export default PostulationStatusPage;
