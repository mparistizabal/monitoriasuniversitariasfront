import React from 'react';
import Header from '../Components/Atoms/Header';
import Title from '../Components/Atoms/Title';
import CreateLine from '../Components/Templates/CreateLine';

const createLearning= () => {
  return (
    <>
      <Header />
      <Title text="Formulario de linea de aprendizaje"/>
      <CreateLine />
    </>
  );
};

export default createLearning;
