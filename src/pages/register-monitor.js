import React from 'react';
import Header from '../Components/Atoms/Header';
import Title from '../Components/Atoms/Title';
import RegistryForm from '../Components/Molecules/RegistryForm';

const createLearning= () => {
  return (
    <>
      <Header />
      <Title text="Registro Monitor"/>
      <RegistryForm/>
    </>
  );
};

export default createLearning;