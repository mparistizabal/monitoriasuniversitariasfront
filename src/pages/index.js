/* Imports */

/* Components */
import Index from '../Components/Templates/Index';

/* React */
import React from 'react';

/* Index Page */
const IndexPage = () => (
  <>
    <h1>Inicio</h1>
    <Index/>
  </>
);

/* Export */
export default IndexPage;