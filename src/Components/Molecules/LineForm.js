import React, { useEffect } from 'react';
import { useFormik } from 'formik';
//material
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import CustomDialog from '../Atoms/customDialog';
import { navigate } from 'gatsby';

import * as Yup from 'yup';

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(5),
    borderBottom: '0px solid transparent',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(),
  },
  save: {
    margin: theme.spacing(),
    backgroundColor: '#ec5252',
    boxShadow: '1px 2px 5px #777',
    color: '#fff',
    '&:hover': {
      backgroundColor: '#FF3344',
    },
  },
  text: {
    fontWeight: 'bold',
    color: '#555',
    marginTop: theme.spacing(3),
  },
}));
const LineForm = props => {
  const lineId = props.idLine;
  const classes = useStyles();
  const [showDialog, setShowDialog] = React.useState(false);
  const validationSchema = Yup.object().shape({
    name: Yup.string()
      .required('Este campo es requerido')
      .matches(
        /^[_A-zÀ-ÿ0-9]*((-|\s)*[_A-zÀ-ÿ0-9 ])*$/g,
        'No se admiten caracteres especiales o espacios en blanco'
      ),
    description: Yup.string()
      .required('Este campo es requerido')
      .matches(
        /^[_A-zÀ-ÿ0-9]*((-|\s)*[_A-zÀ-ÿ0-9 ])*$/g,
        'No se admiten caracteres especiales o espacios en blanco'
      ),
  });
  const formik = useFormik({
    initialValues: {
      name: '',
      description: '',
    },
    validationSchema,
    onSubmit: values => {
      props.exect(values);
    },
  });

  useEffect(() => {
    if (props.description && props.name) {
      formik.setValues({ name: props.name, description: props.description });
    }
  }, [props]);

  const handleClick = () => {
    navigate('/line-list-page/');
  };
  return (
    <>
      <Paper className={classes.paper}>
        <Container component="main" maxWidth="xs">
          <Typography
            component="h1"
            variant="h5"
            align="center"
            className={classes.text}
          >
            {props.title}
          </Typography>
          <form className={classes.form} onSubmit={formik.handleSubmit}>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  type="text"
                  required
                  inputMode="text"
                  fullWidth
                  id="name"
                  placeholder={props.name}
                  label="Nombre"
                  name="name"
                  onChange={formik.handleChange}
                  value={formik.values.name}
                />
                {formik.touched.name && formik.errors.name ? (
                  <div style={{ color: 'red' }}>{formik.errors.name}</div>
                ) : null}
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  type="text"
                  required
                  fullWidth
                  id="description"
                  placeholder={props.description}
                  label="Descripción"
                  name="description"
                  multiline
                  rowsMax={8}
                  onChange={formik.handleChange}
                  value={formik.values.description}
                />
                {formik.touched.description && formik.errors.description ? (
                  <div style={{ color: 'red' }}>
                    {formik.errors.description}
                  </div>
                ) : null}
              </Grid>
              <Grid
                container
                direction="row"
                justify="center"
                alignItems="center"
              >
                <Button
                  type="submit"
                  size="medium"
                  variant="contained"
                  color="primary"
                  className={classes.save}
                >
                  Guardar
                </Button>
                {showDialog && (
                  <CustomDialog
                    dialogType={false}
                    title={title}
                    content={content}
                    url="/line-list-page/"
                  />
                )}
              </Grid>
            </Grid>
          </form>
        </Container>
      </Paper>
    </>
  );
};
export default LineForm;
