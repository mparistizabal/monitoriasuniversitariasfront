import React from 'react';
import { useFormik } from 'formik';
import TextField from '@material-ui/core/TextField';
import DropDown from '../Atoms/DropDown';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import colombiaDepartments from '../../data/colombia.json';
import academicLevels from '../../data/nivelAcademico.json';
import classExperiences from '../../data/experienciaClases.json';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import * as Yup from 'yup';
import { useState, useEffect } from 'react';
import useLine from '../Atoms/Hooks/useLine'
import useSubject from '../Atoms/Hooks/useSubject'
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import CustomDialog from '../Atoms/customDialog';
import Errors from '../Atoms/errors';
import { useMutation } from '@apollo/client';
import gql from 'graphql-tag';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import useMonitor from '../Atoms/Hooks/useMonitor';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 440,
  },
  paper: {
    marginTop: theme.spacing(8),
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(5),
    borderBottom: '0px solid transparent',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  submit: {
    marginRight: theme.spacing(3),
    height: 40,
  },
  newButton: {
    marginBottom: '20px',
    backgroundColor: '#ec5252',
    border: '1px solid #FF3344',
    height: 40,
    paddingLeft: '15px',
    paddingRight: '15px',
    paddingBottom: '0px',
    paddingTop: '0px',
    color: '#fff',
    fontWeight: 'bold',
    boxShadow: '1px 2px 5px #777',
    '&:hover': {
      backgroundColor: '#FF3344',
    },
  },
  save: {
    margin: theme.spacing(),
    backgroundColor: '#ec5252',
    boxShadow: '1px 2px 5px #777',
    color: '#fff',
    '&:hover': {
      backgroundColor: '#FF3344',
    },
  },
  form: {
    alignItems: 'center',
  },
  text: {
    marginTop: '30px',
    textAlign:'left'
  },
}));

const RegistryForm = () => {
  const [showDialog, setShowDialog] = React.useState(false);
  const [showErrorDialog, setShowErrorDialog] = React.useState(false);
  let departments = Object.keys(colombiaDepartments).map(key => colombiaDepartments[key].departamento);

  let defaultDepartment = departments[0];
  const [department, setDepartment] = React.useState(defaultDepartment);

  let levels = Object.keys(academicLevels).map(key => academicLevels[key].nivelAcademico);
  let defaultLevel = levels[0];
  const [level, setLevel] = React.useState(defaultLevel);

  let experiences = Object.keys(classExperiences).map(key => classExperiences[key].experiencia);

  let defaultExperience = experiences[0];
  const [experience, setExperience] = React.useState(defaultExperience);
  const [informaticTool, setInformaticTool] = React.useState(defaultExperience);

  const [title, setTitle] = React.useState("");
  const [content, setContent] = React.useState("");

  const handleDepartmentCallback = childDepartment => {
    setDepartment(childDepartment);
  };

  const handleLevelCallback = childLevel => {
    setLevel(childLevel);
  };

  const handleExperienceCallback = childExperience => {
    setExperience(childExperience);
  };

  const handleInformaticToolCallback = childInformaticTool => {
    setInformaticTool(childInformaticTool);
  };

  const [flag, setflag] = useState(false)
  const [idLine, setIdLine] = useState(null);
  const [radio, setRadio] = React.useState('true');

  const handleLine = childLine => {
    setIdLine(childLine)
    setParentId(childLine)
    setflag(true);
  }

  

  const ADD_TODO = gql`
    mutation create($input: MonitorInput!){
    createMonitor(input:$input){
        user{
          firstName
          lastName
          residence
          id
          levelEducation
        }
      }
    }
  `;
   const handleChangeRadio = (event) => {
    setRadio(event.target.value);
  };

  const changeHandlerCancel = () => {
    setOpen(false);
  };

  const [createMonitor, { loading, error, data }] = useMutation(ADD_TODO);
  const handleForm = async (values) => {
    var objError = null;
    try {
      const { data } = await createMonitor({ variables: { input: values } });
      setTitle('¡Éxito!');
      setContent('¡Gracias por postularse, pronto te daremos respuesta a su solicitud!');
      setEmail(values.email);
      setShowDialog(true);
    } catch (error) {
      let errorApolo = Object.values(error);        
      if(errorApolo[0].length > 0){
        objError = new Errors(errorApolo[0].[0].code);  
      }
      else{
        objError = new Errors(errorApolo[1].name);  
      }      
      setTitle('¡Error!');
      setContent(objError.showTextError());
      setShowErrorDialog(true);
    }
  };

  const { subjects, setParentId, parentId } = useSubject(1);
  const lines = useLine();

  useEffect(() => {
  }, [lines, subjects])
  const classes = useStyles();
  const validationSchema = Yup.object().shape({
    firstName: Yup.string()
      .required('Este campo es requerido')
      .matches(
        /^[_A-zÀ-ÿ]*((-|\s)*[_A-zÀ-ÿ ])*$/g,
        'No se admiten números, caracteres especiales o espacios en blanco'
      ),
    lastName: Yup.string()
      .required('Este campo es requerido')
      .matches(
        /^[_A-zÀ-ÿ]*((-|\s)*[_A-zÀ-ÿ ])*$/g,
        'No se admiten números, caracteres especiales o espacios en blanco'
      ),
    email: Yup.string()
      .email('Este correo no es válido')
      .required('Required'),
    telephone: Yup.string()
      .required('Este campo es requerido')
      .matches(
        /^[0-9]{10}$/,
        'El teléfono debe tener 10 dígitos y sólo debe contener números'
      ),
    college: Yup.string()
      .required('Este campo es requerido')
      .matches(
        /^[_A-zÀ-ÿ]*((-|\s)*[_A-zÀ-ÿ ])*$/g,
        'No se admiten números, caracteres especiales o espacios en blanco'
      ),
    collegeCareer: Yup.string()
      .required('Este campo es requerido')
      .matches(
        /^[A-Za-zÀ-ÿ][A-Za-zÀ-ÿ ]*$/g,
        'No se admiten números, caracteres especiales o espacios en blanco'
      ),
    careerAverage: Yup.number()
      .positive('El promedio académico no puede ser negativo')
      .lessThan(5.1, 'El promedio académico no puede ser mayor a 5'),
      //.cast('Debe ingresar un número'),
    experience: Yup.string()      
      .matches(
        /^[_A-zÀ-ÿ]*((-|\s)*[_A-zÀ-ÿ ])*$/g,
        'No se admiten números, caracteres especiales o espacios en blanco'
      ),
    informaticTool: Yup.string()      
      .matches(
        /^[_A-zÀ-ÿ]*((-|\s)*[_A-zÀ-ÿ ])*$/g,
        'No se admiten números, caracteres especiales o espacios en blanco'
      ),
    workHour: Yup.number()
      .integer('El número de horas de trabajo semanales debe ser un entero')
      .positive(
        'El número de horas de trabajo semanales no puede ser negativo'
      ),
  });
  const formik = useFormik({
    initialValues: {
      firstName: '',
      lastName: '',
      email: '',
      telephone: '',
      residence: '',
      levelEducation: '',
      college: '',
      collegeCareer: '',
      careerAverage: '',
      experience: '',
      informaticTool: '',
      serviceType: [],
      shortJob: '',
      workHour: '',
      subject: [],
    },
    validationSchema,
    onSubmit: values => {
      values.residence = department;
      values.levelEducation = level;
      values.experience = experience;
      values.informaticTool = informaticTool;
      values.subject.push(parentId);
      handleForm(values);
    },
  });

  const { monitor, setEmail, email} = useMonitor("");

  return (
    <Paper className={classes.paper}>
      <Grid item sm={4}>
         <Typography
            component="h1"
            variant="body1"
            align="justify"
            className={classes.text}
          >
            Buscamos profesionales, licenciados, estudiantes sobresalientes de
            últimos semestres, comprometidos que cuenten con las siguientes
            habilidades:<br></br>- Constante aprendizaje.<br></br>- Mente
            creativa.<br></br>- Innovador.<br></br>- Buenas relaciones
            interpersonales.<br></br>- Respetuoso, servicial, abierto al
            dialogo.<br></br>
        </Typography>
      </Grid>
      <Container component="main" maxWidth="sm">
      <Grid item xs={12}>
              <Typography
                component="h1"
                variant="h5"
                align="center"
                className={classes.text}
              >
                Información Personal
              </Typography>
            </Grid>
        <form className={classes.form} onSubmit={formik.handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                type="text"
                required
                inputMode="text"
                fullWidth
                id="firstName"
                placeholder="Nombres"
                label="Nombres"
                name="firstName"
                onChange={formik.handleChange}
                value={formik.values.firstName}
              />
              {formik.touched.firstName && formik.errors.firstName ? (
                <div style={{ color: 'red' }}>{formik.errors.firstName}</div>
              ) : null}
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                type="text"
                required
                inputMode="text"
                fullWidth
                id="lastName"
                placeholder="Apellidos"
                label="Apellidos"
                name="lastName"
                onChange={formik.handleChange}
                value={formik.values.lastName}
              />
              {formik.touched.lastName && formik.errors.lastName ? (
                <div style={{ color: 'red' }}>{formik.errors.lastName}</div>
              ) : null}
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                type="email"
                required
                inputMode="text"
                fullWidth
                id="email"
                placeholder="Correo"
                label="Correo"
                name="email"
                onChange={formik.handleChange}
                value={formik.values.email}
              />
              {formik.touched.email && formik.errors.email ? (
                <div style={{ color: 'red' }}>{formik.errors.email}</div>
              ) : null}
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                inputMode="text"
                fullWidth
                id="telephone"
                placeholder="Telefono"
                label="Telefono"
                name="telephone"
                onChange={formik.handleChange}
                value={formik.values.telephone}
              />
              {formik.touched.telephone && formik.errors.telephone ? (
                <div style={{ color: 'red' }}>{formik.errors.telephone}</div>
              ) : null}
            </Grid>
            <Grid item xs={12}>
              <DropDown
                id="residence"
                key="residence"
                name="residence"
                items={departments}
                label="Departamento de Residencia"
                value={formik.values.residence}
                parentCallback={handleDepartmentCallback}
                defaultItem={department}
                required
              />

              {formik.touched.residence && formik.errors.residence ? (
                <div style={{ color: 'red' }}>{formik.errors.residence}</div>
              ) : null}
            </Grid>
            <Grid item xs={12}>
              <Typography
                component="h1"
                variant="h5"
                align="center"
                className={classes.text}
              >
                Información Académica
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <DropDown
                id="levelEducation"
                key="levelEducation"
                name="levelEducation"
                items={levels}
                label="Nivel Académico Más Alto"
                value={formik.values.levelEducation}
                parentCallback={handleLevelCallback}
                defaultItem={level}
                required
              />
              {formik.touched.levelEducation && formik.errors.levelEducation ? (
                <div style={{ color: 'red' }}>
                  {formik.errors.levelEducation}
                </div>
              ) : null}
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                fullWidth
                id="college"
                label="Universidad"
                name="college"
                onChange={formik.handleChange}
                value={formik.values.college}
                required
              />
              {formik.touched.college && formik.errors.college ? (
                <div style={{ color: 'red' }}>{formik.errors.college}</div>
              ) : null}
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                fullWidth
                id="collegeCareer"
                label="Carrera Universitaria"
                name="collegeCareer"
                onChange={formik.handleChange}
                value={formik.values.collegeCareer}
                required
              />
              {formik.touched.collegeCareer && formik.errors.collegeCareer ? (
                <div style={{ color: 'red' }}>
                  {formik.errors.collegeCareer}
                </div>
              ) : null}
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                fullWidth
                id="careerAverage"
                label="Promedio académico"
                name="careerAverage"
                onChange={formik.handleChange}
                value={formik.values.careerAverage}
                required
              />
              {formik.touched.careerAverage && formik.errors.careerAverage ? (
                <div style={{ color: 'red' }}>
                  {formik.errors.careerAverage}
                </div>
              ) : null}
            </Grid>
            <Grid item xs={12}>
              <DropDown
                id="line"
                key={'line'}
                name="line"
                items={lines}
                flag={true}
                label="Areas de Aprendizaje"
                defaultItem={parentId}
                value={formik.values.line}
                parentCallback={handleLine}
                required
              />
              {subjects.length > 0 &&
                flag &&
                subjects.map((subject, index) => (
                  <FormControlLabel
                    key={index}
                    control={
                      <Checkbox
                        value={subject.node.id}
                        onChange={formik.handleChange}
                        name="subject"
                      />
                    }
                    label={subject.node.name}
                  />
                ))}
            </Grid>

            <Grid item xs={12}>
              <Typography
                component="h1"
                variant="h5"
                align="center"
                className={classes.text}
              >
                Experiencia
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography
                component="h1"
                variant="h5"
                align="left"
                className={classes.text}
              >
                ¿Tienes experiencia dictando clases?
              </Typography>
              <DropDown
                id="experience"
                key="experience"
                name="experience"
                items={experiences}
                label="Elige"
                value={formik.values.experience}
                parentCallback={handleExperienceCallback}
                defaultItem={experience}
                required
              />
            </Grid>
            <Grid item xs={12}>
              <Typography
                component="h1"
                variant="h5"
                align="left"
                className={classes.text}
              >
                ¿Sabes de manejo de herramientas informáticas?
              </Typography>
              <DropDown
                id="informaticTool"
                key={'informaticTool'}
                name="informaticTool"
                items={experiences}
                label="Elige"
                value={formik.values.informaticTool}
                defaultItem={informaticTool}
                parentCallback={handleInformaticToolCallback}
                required
              />
            </Grid>
            <Grid item xs={12}>
              <Typography
                component="h1"
                variant="h5"
                align="left"
                className={classes.text}
              >
                ¿Que servicios quieres dar en nuestra empresa?
              </Typography>
              <FormControlLabel
                control={
                  <Checkbox
                    value={'Dar clases o asesorias'}
                    onChange={formik.handleChange}
                    name="serviceType"
                  />
                }
                label={'Dar clases o asesorias'}
              />
              <FormControlLabel
                control={
                  <Checkbox
                    value={'Realizar Trabajos academicos'}
                    onChange={formik.handleChange}
                    name="serviceType"
                  />
                }
                label={'Realizar Trabajos academicos'}
              />
            </Grid>
            <Grid item xs={12}>
            <Typography
                component="h1"
                variant="h5"
                align="left"
                className={classes.text}
              >
                ¿Te gustaría hacer trabajos reales de corto alcance?
            </Typography>
            <Typography
              component="h1"
              variant="body1"
              align="justify"
              className={classes.text}
            >  
                Trabajos reales acorde a tu carrera, por ejemplo: Un ingeniero
                de sistemas puede realizar una app, un profesional de lenguas
                modernas puede traducir un articulo. Recuerda que si eres
                estudiante también puedes aplicar con tarifas diferentes
              </Typography>
              <RadioGroup aria-label="gender">
                <FormControlLabel
                  control={
                    <Radio
                      value="true"
                      name="shortJob"
                      onChange={formik.handleChange}
                    />
                  }
                  label={'Sí'}
                />
                <FormControlLabel
                  control={
                    <Radio
                      value={'false'}
                      onChange={formik.handleChange}
                      name="shortJob"
                    />
                  }
                  label={'No'}
                />
              </RadioGroup>
            </Grid>
            <Grid item xs={12}>
              <Typography
                component="h1"
                variant="h5"
                align="left"
                className={classes.text}
              >
                ¿Cuántas horas semanales te gustaría trabajar?
              </Typography>
              <TextField
                variant="outlined"
                fullWidth
                id="workHour"
                label="Escribe"
                name="workHour"
                onChange={formik.handleChange}
                value={formik.values.workHour}
              />
              {formik.touched.workHour && formik.errors.workHour ? (
                <div style={{ color: 'red' }}>{formik.errors.workHour}</div>
              ) : null}
            </Grid>
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="center"
            >
              <Button
                type="submit"
                size="medium"
                variant="contained"
                color="primary"
                className={classes.save}
              >
                Registrar
              </Button>
              {showDialog && (
                <CustomDialog
                  dialogType={false}
                  title={title}
                  content={content}
                  onChangeCancel={changeHandlerCancel}
                  url="/postulation-status/"
                  id={monitor.email}
                  name={monitor.status}
                />
              )}
              {showErrorDialog && (
                <CustomDialog
                  dialogType={false}
                  title={title}
                  content={content}
                  onChangeCancel={changeHandlerCancel}
                  url="/register-monitor/"
                />
              )}
            </Grid>
          </Grid>
        </form>
      </Container>
    </Paper>
  );
};

export default RegistryForm;
