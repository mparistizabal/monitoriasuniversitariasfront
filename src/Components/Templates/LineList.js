import React from 'react';
import { gql, useQuery } from '@apollo/client';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import { useEffect } from 'react';
import { useState } from 'react';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import ListIcon from '@material-ui/icons/List';
import { navigate } from 'gatsby';
import { useMutation } from '@apollo/client';
import CustomDialog from '../Atoms/customDialog';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Errors from '../Atoms/errors';

const GET_LINE = gql`
  {
    allSubjects(parentId_Isnull: true) {
      edges {
        node {
          name
          id
          description
        }
      }
    }
  }
`;

const DELETE_LINE = gql`
  mutation deleteLine($input: ID!) {
    deleteSubject(input: $input) {
      subject {
        id
        name
        description
      }
    }
  }
`;

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 440,
  },
  paper: {
    padding: theme.spacing(5),
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    margin: 'auto',
    width: '85%',
  },
  submit: {
    marginRight: theme.spacing(3),
    height: 40,
  },
  newButton: {
    marginBottom: '20px',
    backgroundColor: '#ec5252',
    border: '1px solid #FF3344',
    height: 40,
    paddingLeft: '15px',
    paddingRight: '15px',
    paddingBottom: '0px',
    paddingTop: '0px',
    color: '#fff',
    fontWeight: 'bold',
    boxShadow: '1px 2px 5px #777',
    '&:hover': {
      backgroundColor: '#FF3344',
    },
  },
}));
const LineList = () => {
  const [open, setOpen] = React.useState(false);
  const [openConfirm, setOpenConfirm] = React.useState(false);
  const classes = useStyles();
  const [
    deleteLine,
    { loading: mutationLoading, error: mutationError, data: mutationData },
  ] = useMutation(DELETE_LINE);
  const [title, setTitle] = React.useState('');
  const [content, setContent] = React.useState('');

  const [page, setPage] = useState(0);
  const { loading, error, data } = useQuery(GET_LINE, {
    pollInterval: 1000,
    notifyOnNetworkStatusChange: true,
  });
  const [lines, setlines] = useState([]);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [rowsCount, setRowsCount] = React.useState(0);
  const [idLine, setIdLine] = React.useState('');

  useEffect(() => {
    if (data) {
      setlines(data.allSubjects.edges);
    }
    setRowsCount(lines.length);
  }, [data, lines, openConfirm]);

  const handleClickAway = () => {
    setOpenConfirm(false);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const handleEdit = id => {
    navigate('/edit-line/', { state: { idLine: id } });
  };

  const changeHandler = async id => {
    const { data } = deleteLine({ variables: { input: idLine } });
    if (!mutationError) {
      setTitle('¡Éxito!');
      setContent('¡La línea de aprendizaje se ha eliminado!');
    } else {
      let errorApolo = Object.values(mutationError);      
      var objError = new Errors(errorApolo[0].[0].code);
      setTitle('¡Error!');
      setContent(objError.showTextError());
    }
    setOpen(false);
    setOpenConfirm(true);
  };

  const changeHandlerCancel = () => {
    setOpen(false);
  };

  const handleDelete = async id => {
    setTitle('Confirmación');
    setContent('¿Desea eliminar la linea de aprendizaje?');
    setIdLine(id);
    handleClickOpen();
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleCreate = () => {
    navigate('/create-line');
  };

  const handleList = (id, name) => {
    navigate('/subject-list/', { state: { id: id, name: name } });
  };

  const columns = [
    {
      id: 'nameLine',
      label: 'Línea de Aprendizaje',
      minWidth: 170,
      align: 'center',
    },
    {
      id: 'editLine',
      label: '',
      minWidth: 50,
      align: 'center',
    },
    {
      id: 'deleteLine',
      label: '',
      minWidth: 50,
      align: 'center',
    },
    {
      id: 'theme',
      label: 'Tema',
      minWidth: 170,
      align: 'center',
    },
  ];
  return (
    <Grid container spacing={0} direction="row">
      <Paper className={classes.paper}>
        <Grid item xs={12} sm container>
          <Grid item xs={12} sm container justify="flex-end" direction="row">
            <Button
              startIcon={<AddIcon />}
              size="small"
              className={classes.newButton}
              onClick={handleCreate}
            >
              Crear Línea
            </Button>
          </Grid>
          <TableContainer className={classes.container}>
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  {columns.map(column => (
                    <TableCell
                      key={column.id}
                      align={column.align}
                      style={{ minWidth: column.minWidth }}
                    >
                      {column.label}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {lines
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((line, index) => {
                    return (
                      <TableRow
                        hover
                        role="checkbox"
                        id={`row-${index}`}
                        key={`item-${line.node.name}-${index}`}
                      >
                        <TableCell align="center">{line.node.name}</TableCell>
                        <TableCell align="center">
                          <Button
                            id={`btn-edit-${line.node.name}-${index}`}
                            startIcon={<EditIcon />}
                            className={classes.margin}
                            size="small"
                            color="inherit"
                            onClick={e => handleEdit(line.node.id, e)}
                          ></Button>
                        </TableCell>
                        <TableCell align="center">
                          <Button
                            id={`btn-delete-${line.node.name}-${index}`}
                            startIcon={<DeleteIcon />}
                            className={classes.margin}
                            size="small"
                            color="inherit"
                            onClick={e => {
                              handleDelete(line.node.id, e);
                            }}
                          ></Button>
                        </TableCell>
                        <TableCell align="center">
                          <Button
                            id={`btn-subjects-${line.node.name}-${index}`}
                            startIcon={<ListIcon />}
                            className={classes.margin}
                            size="small"
                            color="inherit"
                            onClick={e =>
                              handleList(line.node.id, line.node.name, e)
                            }
                          ></Button>
                        </TableCell>
                      </TableRow>
                    );
                  })}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
        <TablePagination
          labelRowsPerPage="Filas por página"
          labelDisplayedRows={({ from, to, count }) => `${from}-${to} de ${count}`}
          component="div"
          count={rowsCount}
          page={page}
          onChangePage={handleChangePage}
          rowsPerPage={rowsPerPage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
      {open && (
        <CustomDialog
          dialogType={true}
          title={title}
          content={content}
          onChange={changeHandler}
          onChangeCancel={changeHandlerCancel}
        />
      )}
      <ClickAwayListener
        mouseEvent="onMouseDown"
        touchEvent="onTouchStart"
        onClickAway={handleClickAway}
      >
        <div>
          {openConfirm && (
            <CustomDialog
              dialogType={false}
              title={title}
              content={content}
              onChange={changeHandler}
              onChangeCancel={changeHandlerCancel}
            />
          )}
        </div>
      </ClickAwayListener>
    </Grid>
  );
};
export default LineList;
