import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { navigate } from 'gatsby'
import BookIcon from '@material-ui/icons/Book';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 440,
  },
  paper: {
    padding: theme.spacing(5),
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    margin: 'auto',
    width: '85%',
  },
}));

const Index = () => {
  const classes = useStyles();
  
  const handleLine = () => {
    navigate("/line-list-page/");
  };

  return (
    <Grid container spacing={0} direction="row">
      <Paper className={classes.paper}>
        <Button
          variant="outlined"
          startIcon={<BookIcon />}
          size="small"
          color="inherit"
          onClick={handleLine}
        >
          Ir a Líneas de Aprendizaje
        </Button>
      </Paper>
    </Grid>
  );
}
export default Index;