import React from 'react';
import gql from 'graphql-tag';
import { useMutation } from '@apollo/client';
import CustomDialog from '../Atoms/customDialog';
import SubjectForm from '../Molecules/SubjectForm'
import { navigate } from 'gatsby';
import Errors from '../Atoms/errors';

const ADD_TODO = gql`
 mutation createSubject($input: CreateSubjectInput!){
    createSubject(input:$input){
      subject{
        id
        name
        description
      }
    }
  }
`;
const TCreateSubject = props => {
  const [createSubject, { loading, error, data }] = useMutation(ADD_TODO);
  const [showDialog, setShowDialog] = React.useState(false);
  const [title, setTitle] = React.useState("");
  const [content, setContent] = React.useState("");
  const objectID = { parentId: props.idLine }

  const handleForm = async (values) => {
    values = Object.assign(values, objectID);
    try {
      const { data } = await createSubject({ variables: { input: values } });
      setTitle('¡Éxito!');
      setContent('¡Creación Exitosa!');
      //navigate('/subject-list/', { state: { id: props.idLine, name: props.nameLine} });
    } catch (error) {
      let errorApolo = Object.values(error);      
      var objError = new Errors(errorApolo[0].[0].code);
      setTitle('¡Error!');
      setContent(objError.showTextError());      
    }
    setShowDialog(true);
  };

  const changeHandlerCancel = () => {
    setShowDialog(false);
  };
  return (
    <>
      <SubjectForm exect={handleForm} title="Crear Tema" nameLine={props.nameLine} />
      {showDialog && (
        <CustomDialog
          dialogType={false}
          title={title}
          content={content}
          url="/subject-list/"
          id={props.idLine}
          name={props.nameLine}
          onChangeCancel={changeHandlerCancel}
      />
      )}
    </>
  );
}
export default TCreateSubject;