import React from "react"
import { useEffect } from 'react'
import { useState } from 'react';
import { gql, useQuery } from '@apollo/client'
import { useMutation } from '@apollo/client'
import CustomDialog from '../Atoms/customDialog';
import SubjectForm from '../Molecules/SubjectForm';
import Errors from '../Atoms/errors';

const GET_SUBJECT = gql`
   query getSubjectByID($id: ID!){
    subject(id: $id){
        name
        id
        description,
    }
}
`;

const EDIT_SUBJECT = gql`
 mutation editSubject($input : UpdateSubjectInput!){
    updateSubject(input:$input){
        subject{
            description
            name
            id
            }
    }
}
`;

const EditSubject = props => {
    const [showDialog, setShowDialog] = React.useState(false);
    const [title, setTitle] = React.useState('');
    const [content, setContent] = React.useState('');
    let id = props.idSubject;
    const objectID = { id: id };
    const [subject, setSubject] = useState([]);
    const { loading, error, data } = useQuery(GET_SUBJECT, {
        variables: { id},
    });
    useEffect(() => {
        if (data) {
          setSubject(data.subject);
        }
      }, [data])

    const [editSubject, { loading: mutationLoading, errors: mutationError, data: mutationData }] = useMutation(EDIT_SUBJECT);
    const changeHandlerCancel = () => {
        setShowDialog(false);
      };
    const handleForm = async (values) => {    
        values = Object.assign(values, objectID);
        try {
            const { data } = await editSubject({ variables: { input: values } });
            setTitle("¡Éxito!");
            setContent("¡Línea de Tema actualizada con éxito!");

        } catch (mutationError) {
            let errorApolo = Object.values(mutationError);      
            var objError = new Errors(errorApolo[0].[0].code);
            setTitle('¡Error!');
            setContent(objError.showTextError());
        }
        setShowDialog(true);
    }
    return (
        <>
            <SubjectForm exect={handleForm} title="Editar Tema" nameLine={props.nameLine}  name = {subject.name} description={subject.description}/>
            {showDialog && (
                <CustomDialog
                    dialogType={false}
                    title={title}
                    content={content}
                    url="/subject-list/"
                    id={props.parentId}
                    name={props.nameLine}
                    onChangeCancel={changeHandlerCancel}
                />
            )}
        </>
    );
}
export default EditSubject;