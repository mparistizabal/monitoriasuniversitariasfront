import React from "react"
import { useEffect } from 'react'
import { useState } from 'react';
import { gql, useQuery } from '@apollo/client'
import { useMutation } from '@apollo/client'
import LineForm from '../Molecules/LineForm';
import CustomDialog from '../Atoms/customDialog';
import Errors from '../Atoms/errors';

const GET_LINE = gql`
  query getSubjectByID($id: ID!){
  subject(id: $id){
      name
      id
      description,
  }
}
`;


const EDIT_LINE = gql`
  mutation updateSubject($input: UpdateSubjectInput!){
    updateSubject(input: $input){
      subject{
        id
        name
        description
      }
    }
  }
`;

const EditLine = props => {
  const [title, setTitle] = React.useState('');
  const [content, setContent] = React.useState('');
  const [showDialog, setShowDialog] = React.useState(false);

  let id = props.idLine;
  const { loading, error, data } = useQuery(GET_LINE, {
    variables: { id },
  });

  const [line, setline] = useState([]);
  useEffect(() => {
    if (data) {
      setline(data.subject);
    }
  }, [data])
  //mutacion para editar a linea de aprenziaje
  const [editLine, { loading: mutationLoading, errors: mutationError, data: mutationData }] = useMutation(EDIT_LINE);

  const objectID = { id: props.idLine }
  
  const handleForm = async (values) => {
    values = Object.assign(values, objectID);
    try {
      const { data } = await editLine({ variables: { input: values } });
      setTitle("¡Éxito!");
      setContent("¡Línea de Aprendizaje actualizada con éxito!");
    } catch (mutationError) {
      let errorApolo = Object.values(mutationError);      
      var objError = new Errors(errorApolo[0].[0].code);
      setTitle('¡Error!');
      setContent(objError.showTextError());
    }
    setShowDialog(true);
  }
  if (error) {
    
    setTitle("¡Error!");
    setContent(error.message);
  }
  return (
    <>
      <LineForm exect={handleForm} name={line.name} description={line.description} title="Editar Línea de Aprendizaje" />
      {showDialog && (
        <CustomDialog
          dialogType={false}
          title={title}
          content={content}
          url="/line-list-page/"
        />
      )}
    </>
  );
};

export default EditLine;