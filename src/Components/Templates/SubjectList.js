import React from 'react';
import { gql, useQuery } from '@apollo/client'
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow'
import { useEffect } from 'react'
import { useState } from 'react';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import ListIcon from '@material-ui/icons/List';
import { navigate } from 'gatsby'
import Typography from '@material-ui/core/Typography'
import CustomDialog from "../Atoms/customDialog"
import { useMutation } from '@apollo/client'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Errors from '../Atoms/errors';

const GET_LINE = gql`
query getSubjects($parentId: ID){
  allSubjects(parentId: $parentId){
    edges{
      node{
        id
        name
        description
      }
    }
  }
}`

const DELETE_SUBJECT = gql`
  mutation deleteSubject($input: ID!) {
    deleteSubject(input: $input) {
      subject {
        id
        name
        description
      }
    }
  }
`;

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 440,
  },
  paper: {
    padding: theme.spacing(5),
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    margin: 'auto',
    width: '85%',
  },
  submit: {
    marginRight: theme.spacing(3),
    height: 40,
  },
  newButton: {
    marginBottom: '20px',
    backgroundColor: '#ec5252',
    border: '1px solid #FF3344',
    height: 40,
    paddingLeft: '15px',
    paddingRight: '15px',
    paddingBottom: '0px',
    paddingTop: '0px',
    color: '#fff',
    fontWeight: 'bold',
    boxShadow: '1px 2px 5px #777',
    '&:hover': {
      backgroundColor: '#FF3344',
    },
  },
}));
const SubjectList = props => {
  let name = props.name;
  let parentId = props.id;
  const classes = useStyles();
  const [page, setPage] = useState(0);
  const { loading, error, data } = useQuery(GET_LINE,{
    variables: {parentId} , pollInterval: 1000, notifyOnNetworkStatusChange: true
  });
  const [deleteSubject, { loading:mutationLoading,error:mutationError ,data:mutationData}] = useMutation(DELETE_SUBJECT);
  const [open, setOpen] = React.useState(false);
  const [openConfirm, setOpenConfirm] = React.useState(false);
  const [title, setTitle] = React.useState("");
  const [content, setContent] = React.useState("");
  const [showDialog, setShowDialog] = React.useState(false);
  const [subjects, setSubject]= useState([]);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [rowsCount, setRowsCount] = React.useState(0);
  const [idSubject, setIdSubject] = React.useState('');
  
  useEffect(() => {
    if (data) {
      setSubject(data.allSubjects.edges);
    }
    setRowsCount(subjects.length);
  }, [data, subjects])  

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClickAway = () => {
    setOpen(false);
    setOpenConfirm(false);
  };

  const changeHandler = async id => {
    const {data} = deleteSubject({ variables:{input:idSubject}})
    if (!mutationError) {
      setTitle('¡Éxito!');
      setContent('¡El tema se ha eliminado!');
    } else {
      let errorApolo = Object.values(mutationError);      
      var objError = new Errors(errorApolo[0].[0].code);
      setTitle('¡Error!');
      setContent(objError.showTextError());
    }
    setOpen(false);
    setOpenConfirm(true);
  }

  const changeHandlerCancel = () => {
    setOpen(false);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const handleEdit = (idSubject, ) =>{
    navigate("/edit-subject/", {state: {nameLine: props.name, idSubject: idSubject, parentId:parentId} })
  }
  const handleDelete = async (id) => {
    setTitle('Confirmación');
    setContent('¿Desea eliminar el tema?');
    setIdSubject(id);
    handleClickOpen();
  }

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleCreate = () => {
    navigate("/create-subject/", {state: {idLine: props.id, nameLine: props.name}});
  };

  const handleBack = () => {
    navigate("/line-list-page/");
  };

  const columns = [
    { 
      id: 'subjectName', 
      label: 'Tema:', 
      minWidth: 170, 
      align: 'center'
    },
    { 
      id: 'editsubject', 
      label: '', 
      minWidth: 50, 
      align: 'center'
    },
    {
      id: 'deletesubject',
      label: '',
      minWidth: 50,
      align: 'center',
    },
  ];
  return (
    <Grid container spacing={0} direction="row">
      <Paper className={classes.paper}>
        <Grid item xs={12} sm container>
          <Grid item xs={12} sm container justify="flex-end" direction="row">
            <Button
              startIcon={<AddIcon />}
              size="small"
              className={classes.newButton}
              onClick={handleCreate}
            >
              Crear Tema
            </Button>
          </Grid>
          <TableContainer className={classes.container}>
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  {columns.map(column => (
                    <TableCell
                      key={column.id}
                      align={column.align}
                      style={{ minWidth: column.minWidth }}
                    >
                      {column.label}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {subjects
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((subject, index) => {
                    return (
                      <TableRow
                        hover
                        role="checkbox"
                        id={`row-${index}`}
                        key={`item-${subject.node.name}-${index}`}
                      >
                        <TableCell align="center">
                          {subject.node.name}
                        </TableCell>
                        <TableCell align="center">
                          <Button
                            id={`btn-edit-${subject.node.name}-${index}`}
                            startIcon={<EditIcon />}
                            className={classes.margin}
                            size="small"
                            color="inherit"
                            onClick={e => handleEdit(subject.node.id, e)}
                          ></Button>
                        </TableCell>
                        <TableCell align="center">
                          <Button
                            id={`btn-delete-${subject.node.name}-${index}`}
                            startIcon={<DeleteIcon />}
                            className={classes.margin}
                            size="small"
                            color="inherit"
                            onClick={e => handleDelete(subject.node.id, e)}
                          ></Button>
                        </TableCell>
                      </TableRow>
                    );
                  })}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
        <TablePagination
          labelRowsPerPage="Filas por página"
          labelDisplayedRows={({ from, to, count }) => `${from}-${to} de ${count}`}
          component="div"
          count={rowsCount}
          page={page}
          onChangePage={handleChangePage}
          rowsPerPage={rowsPerPage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
        {open && (
          <CustomDialog
            dialogType={true}
            title={title}
            content={content}
            onChange={changeHandler}
            onChangeCancel={changeHandlerCancel}
          />
        )}
        <ClickAwayListener
          mouseEvent="onMouseDown"
          touchEvent="onTouchStart"
          onClickAway={handleClickAway}
        >
          <div>
          {openConfirm && (
            
              <CustomDialog
                dialogType={false}
                title={title}
                content={content}
                onChange={changeHandler}
                onChangeCancel={changeHandlerCancel}
                id={parentId}
                name={name}
                url="/subject-list/"
              />
          )}
          </div>
        </ClickAwayListener>
      </Paper>
    </Grid>
  );
}
export default SubjectList;