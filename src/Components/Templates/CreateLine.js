import React from 'react';
import gql from 'graphql-tag';
import { useMutation } from '@apollo/client';
import LineForm from '../Molecules/LineForm';
import CustomDialog from '../Atoms/customDialog';
import Errors from '../Atoms/errors';

const ADD_TODO = gql`
  mutation createSubject($input: CreateSubjectInput!){
  createSubject(input: $input){
    subject{
      id
      name
      description
    }
  }
}
`;

const CreateLine = () => {
  const [title, setTitle] = React.useState("");
  const [content, setContent] = React.useState("");
  const [showDialog, setShowDialog] = React.useState(false);
  const [
    createLine,
    { loading, error, data },
  ] = useMutation(ADD_TODO);

  const handleForm = async (values) => {
    try {
      const { data } = await createLine({ variables: { input: values } });
      setTitle('¡Éxito!');
      setContent('¡Creación Exitosa!');
      
    } catch (error) {
      let errorApolo = Object.values(error);      
      var objError = new Errors(errorApolo[0].[0].code);
      setTitle('¡Error!');
      setContent(objError.showTextError());
    }
    setShowDialog(true);
  };


  return (
    <>
      <LineForm exect={handleForm} title="Crear Línea de Aprendizaje" />
      {showDialog && (
        <CustomDialog
          dialogType={false}
          title={title}
          content={content}
          url="/line-list-page/"
        />
      )}
    </>
  );
}
export default CreateLine;