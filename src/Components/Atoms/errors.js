class Errors {
  constructor(textError) {
    this.textError = textError;
  }

  showTextError() {    
    switch (this.textError) {
      case 'NameAlreadyUsed':
        return 'Este nombre ya ha sido utilizado';
      case 'ParentSubjectNotFound':
        return('No se encontró al tema padre');
      case 'ParentLearningLineNotFound':
        return('No se encontró a la línea de aprendizaje padre');
      case 'SubjectCircularRealtion':
        return('No sé qué hace este error :v');
      case 'MaxDeepSubject':
        return('Se alcanzó la máxima profundidad');
      case 'ValidationError':
        return('No se pudo validar');
      case 'IntegrityError':
        return('Este correo ya ha sido utilizado');
      case 'ServerError':
        return('Error del servidor');
      default:
        return('Ocurrió un error, intente más tarde');
    }
  }
}

export default Errors;