import React from 'react';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  titleContainer: {
    marginTop: 20,
    //padding: '90px 10px auto 40px',
    paddingBottom: 15,
    paddingTop: 5,
    color: '#fff',
    width: 'auto',
    maxWidth: '60%',
    height: 0,
    borderBottomWidth: 100,
    borderBottomColor: '#ec5252',
    borderRightWidth: 60,
    borderLeftWidth: 0,
    borderRightColor: 'transparent',
    borderLeftColor: 'transparent',
    borderTopColor: 'transparent',
    borderStyle: 'solid',
  },
  wrapper: {
    //height: "auto",
    flexDirection: "column",
    display: "flex",
    justifyContent: "center",
    //backgroundColor: "blue",
    marginTop: 10,
    marginBottom: 10,
  },
  title: {
    marginTop: 35,
    marginLeft: 30,
    fontSize: '135%',
  },
}));

const Title = ({ text }) => {
  const styles = useStyles();
  return (
    <div className={styles.titleContainer}>
      <div className={styles.wrapper}>
        <h2 className={styles.title}>{text || ''}</h2>
      </div>
    </div>
  );
};

export default Title;
