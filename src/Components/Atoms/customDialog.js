/* Imports */

/* Gatsby */
import { navigate } from 'gatsby';

/* Material UI */
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { makeStyles } from '@material-ui/core/styles';

/* React */
import React from 'react';

/* CSS Styles */
const useStyles = makeStyles(theme => ({
  text: {
    marginTop: theme.spacing(8),
  },
}));

/* Custom Modal Window To Show Messages To Users */
export default function CustomDialog(props) {
  const [open, setOpen] = React.useState(true);

  const changeHandler = () => {
    props.onChange();
  }

  const changeHandlerCancel = () => {
    props.onChangeCancel();
  }

  const handleClose = () => {
    setOpen(false);
    if (props.id) {
      navigate(props.url, { state: { id: props.id, name: props.name } });
    } else {
      navigate(props.url);
    }
  };

  let button;
  if (props.dialogType) {
    button = (
      <>
        <Button onClick={changeHandler} color="primary">
          Aceptar
        </Button>
        <Button onClick={changeHandlerCancel} color="primary">
          Cancelar
        </Button>
      </>
    );
  } else {
    button = (
      <>
        <Button onClick={handleClose} color="primary">
          Aceptar
        </Button>
      </>
    );
  }

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{props.title}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {props.content}
          </DialogContentText>
        </DialogContent>
        <DialogActions>{button}</DialogActions>
      </Dialog>
    </div>
  );
}
