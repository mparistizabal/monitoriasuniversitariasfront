import React, { useState } from 'react';

import { navigate } from 'gatsby'

import { TextField, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  base: {
    backgroundColor: '#f6f6f6',
    display: 'flex',
    flexDirection: 'column',
    borderRadius: 10,
    padding: 20,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  field: {
    backgroundColor: "#fff",
    marginTop: 10,
    marginBottom: 10,
  },
  buttonSignUp: {
    backgroundColor: "#fff",
    marginTop: 20,
    paddingLeft: "15px",
    paddingRight: "15px",
    fontWeight: "bold",
    color: '#032440',
    fontSize: 13,
    border: "1px solid #032440",
    paddingTop: 10,
    boxShadow: "1px 2px 5px #777",
  },
  buttonLogin: {
      backgroundColor: "#ec5252",
      marginTop: 20,
      paddingLeft: "15px",
      paddingRight: "15px",
      fontWeight: "bold",
      color: "#fff",
      fontSize: 13,
      paddingTop: 10,
      boxShadow: "1px 2px 5px #777",
      '&:hover': {
        backgroundColor: "#FF3344"
      },
  }
}));

const LoginCard = () => {
  const styles = useStyles();
  const [email, setEmail] = useState('');
  const [passwd, setPasswd] = useState('');

  return (
    <div className={styles.base}>
      <TextField
        className={styles.field}
        label="Correo electronico"
        variant="outlined"
        value={email}
        onChange={event => setEmail(event.target.value)}
      />
      <TextField
        className={styles.field}
        label="Contraseña"
        variant="outlined"
        value={passwd}
        onChange={event => setPasswd(event.target.value)}
      />
      <Button className={styles.buttonLogin}>Ingresar</Button>
      <Button className={styles.buttonSignUp} onClick={() => navigate("/register-monitor/")}>Registrarme como monitor</Button>
    </div>
  );
};

export default LoginCard;
