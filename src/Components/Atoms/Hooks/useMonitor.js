import { gql, useQuery } from '@apollo/client';
import { useState, useEffect } from 'react';

const GET_MONITOR = gql`
  query getMonitors($email: String) {
    allMonitors(email: $email) {
      edges {
        node {
          email
          status
        }
      }
    }
  }
`;

const UseMonitor = (monitorEmail) => {
    
  const [email, setEmail]= useState(monitorEmail);
    
  const { loading, error, data } = useQuery(GET_MONITOR, {
      variables: {email},
      notifyOnNetworkStatusChange: true,
    });

  const [monitor, setMonitor] = useState([]);
    useEffect(() => {
     if(data){
       setMonitor(data.allMonitors.edges[0].node)
     } 
    }, [data,monitor,email])
    return {
      monitor,setEmail,email
    };
}

export default UseMonitor;