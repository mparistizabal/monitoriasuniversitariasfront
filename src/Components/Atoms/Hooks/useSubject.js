import { gql, useQuery } from '@apollo/client';
import {useState , useEffect} from 'react';

const GET_SUBJECT = gql`
query getSubjects($parentId: ID){
  allSubjects(parentId: $parentId){
    edges{
      node{
        id
        name
        description
      }
    }
  }
}`

const UseSubject = (idLine) =>{
  
    const [parentId, setParentId]= useState(idLine);
    
    const { loading, error, data } = useQuery(GET_SUBJECT, {
        variables: {parentId},
        notifyOnNetworkStatusChange: true,
      });
    const [subjects, setSubjects] = useState([]);
      useEffect(() => {
      
       if(data){
         setSubjects(data.allSubjects.edges)
       } 
      }, [data,subjects,parentId])
      return {
        subjects,setParentId,parentId
      };
}
export default UseSubject;