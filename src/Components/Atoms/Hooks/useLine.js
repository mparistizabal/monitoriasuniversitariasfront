import { gql, useQuery } from '@apollo/client';
import {useState , useEffect} from 'react';
const GET_LINE = gql`
{
  allSubjects(parentId_Isnull: true) {
    edges {
      node {
        name
        id
        description
      }
    }
  }
}
`;
const UseLine =() =>{
    const { loading, error, data } = useQuery(GET_LINE, {
        //pollInterval: 1000,
        notifyOnNetworkStatusChange: true,
      });
    const [lines, setlines] = useState([]);
      useEffect(() => {
       if(data){
         setlines(data.allSubjects.edges)
       }  
      }, [data,lines])
      return (lines);
}
export default UseLine;