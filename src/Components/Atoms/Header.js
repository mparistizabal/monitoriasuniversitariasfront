import React from 'react';

import { AppBar, Toolbar, IconButton, Button } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import { makeStyles } from '@material-ui/core/styles';
import { navigate } from 'gatsby'

import Logo from '../../assets/Logo-white.jpg';

const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1,
    backgroundColor: '#fff',
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  logo: {
    maxHeight: '50px',
  },
  buttonsContainer: {
    marginLeft: '20px',
    display: 'flex',
    flex: '1',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  buttonLogin: {
      backgroundColor: "#ec5252",
      marginRight: "10px",
      marginLeft: "10px",
      paddingLeft: "15px",
      paddingRight: "15px",
      fontWeight: "bold",
      color: "#fff",
      boxShadow: "1px 2px 5px #777",
      '&:hover': {
        backgroundColor: "#FF3344"
      },
  }
}));

const Header = () => {
  const styles = useStyles();

  return (
    <div className={styles.grow}>
      <AppBar classes={{ root: styles.grow }} position="static">
        <Toolbar>
          <IconButton
            edge="start"
            className={styles.menuButton}
            color="default"
            aria-label="open options"
          >
            <MenuIcon />
          </IconButton>
          <img className={styles.logo} src={Logo} alt="logo" />
          <div className={styles.buttonsContainer}>
            <Button>Inicio</Button>
            <Button className={styles.buttonLogin} onClick={() => navigate("/login/")}>Iniciar sesión</Button>
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default Header;
