import React from 'react';
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

const useStyles = makeStyles((theme) => ({
  margin: {
    margin: theme.spacing(1)
  },
  formControl: {
    margin: 0,
    fullWidth: true,    
    display: 'flex',
    wrap: 'nowrap'
  },
}));

function DropDown(props) {

  let items = props.items;
  let label = props.label;

  const classes = useStyles();
  const [selectedItem, setSelectedItem] = React.useState(props.defaultItem);

  const onTrigger = (event) => {
    setSelectedItem(event.target.value);
    props.parentCallback(event.target.value);
  }

  return (
    <>
      <div>
        <h1>{props.children}</h1>
        <FormControl variant="outlined" className={classes.formControl}>
          <InputLabel id="item-label" className={classes.input}>
            {label}
          </InputLabel>
          <Select            
            labelId="item-label"
            id="item"
            value={selectedItem}
            onChange={onTrigger}
            label={label}
            defaultValue = {selectedItem}
            fullWidth
            required
          >
            {props.flag && items.map((item, index) =><MenuItem key={index} value={item.node.id}>
                {item.node.name} 
              </MenuItem>)}

            {!props.flag && items.map((item, index) => (
              <MenuItem key={index} value={item}>
                {item}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </div>
    </>
  );
}

export default DropDown;